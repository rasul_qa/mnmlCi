import supertest from "supertest";
import urls from "../config";

const check = {
  get: async (access_key, email) => {
    const res = await supertest(urls.mailboxlayer)
      .get('api/check')
      .query({access_key: access_key, email: email})
    return res
  }
}

export default check
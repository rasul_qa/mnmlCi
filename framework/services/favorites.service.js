import supertest from "supertest";
import urls from "../config";

const favorites = {
  get: async (token, id = '') => {
    const res = await supertest(urls.airportgap)
      .get('api/favorites/'+ `${id}`)
      .set({'Authorization': "Token " + `${token}`})
    return res
  },
  post: async (token, body) => {
    const res = await supertest(urls.airportgap)
      .post('api/favorites/')
      .send(body)
      .set({'Authorization': "Bearer " + `${token}`})
    return res
  },
  patch: async (token, id, body) => {
    const res = await supertest(urls.airportgap)
      .patch('api/favorites/' + `${id}`)
      .send(body)
      .set({'Authorization': "Bearer " + `${token}`})
    return res
  },
  delete: async (token, id) => {
    const res = await supertest(urls.airportgap)
      .delete('api/favorites/' + `${id}`)
      .set({'Authorization': "Bearer " + `${token}`})
    return res
  }
}

export default favorites
import supertest from "supertest";
import urls from "../config";

const token = {
  post: async (body) => {
    const res = await supertest(urls.airportgap)
      .post('api/tokens')
      .send(body)
    return res
  }
}

export default token
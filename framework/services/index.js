import token from "./token.service";
import favorites from "./favorites.service"
import check from "./check.service"

const api = () => ({
  token: () => ({ ...token }),
  favorites: () => ({ ...favorites }),
  check: () => ({ ...check })
})

export default api
import api from "../framework/services/index"
let access_key = '7b7faeea8921c9a6260481c47b681ac6'
let email = 'test@airportgap.com'
describe('/check', () => {
  test('Проверка правильного email', async () => {
    const res = await api().check().get(access_key, email)
    expect(res.status).toBe(200)
    expect(res.body.format_valid).toBe(true)
  })

  test.each`
  email | error_code
  ${"testairportgap.com"} | ${211}
  ${"test@"} | ${211}
  ${""} | ${210}
  `(`Получаем код ошибки $error_code при не правильном email: '$email'`, async({email, error_code}) => {
      const res = await api().check().get(access_key, email)
      expect(res.body.error.code).toEqual(error_code)
  })

  test('Получаем ошибку при запросе без api_key', async () => {
    const res = await api().check().get('', email)
    expect(res.status).toBe(200)
    expect(res.body.error.code).toBe(101)
  })
})
const sumResultGroup = (scores) => {
  let result = 0
  for(let key in scores) {
    if(scores[key]) {
      if(scores[key] < 0) {
        continue
      }
      result += Number(scores[key])
    }
  }
  return result
}

describe('Тест функции sumResultGroup для получения суммы баллов группы', () => {
  test('Сумма всех целых баллов равна 16', () => {
    let scores = {
      Anna: 10,
      Olga: 1,
      Ivan: 5,
    }
    expect(sumResultGroup(scores)).toBe(16)
  })
  test('Сумма всех баллов с плавающей точкой равна 14.599999999999998', () => {
    let scores = {
      Anna: 10.1,
      Olga: 1.2,
      Ivan: 3.3,
    }
    expect(sumResultGroup(scores)).toBe(14.599999999999998)
  })
  test('Сумма всех баллов cо строкой равна 30', () => {
    let scores = {
      Anna: 10,
      Olga: 10,
      Ivan: '10',
    }
    expect(sumResultGroup(scores)).toBe(30)
  })
  test('Сумма всех баллов c null равна 20', () => {
    let scores = {
      Anna: 10,
      Olga: 10,
      Ivan: null,
    }
    expect(sumResultGroup(scores)).toBe(20)
  })
  test('Сумма всех баллов c undefined равна 20', () => {
    let scores = {
      Anna: 10,
      Olga: 10,
      Ivan: undefined,
    }
    expect(sumResultGroup(scores)).toBe(20)
  })
  test('Сумма всех баллов c отрицательным значением равна 20', () => {
    let scores = {
      Anna: 10,
      Olga: 10,
      Ivan: -10,
    }
    expect(sumResultGroup(scores)).toBe(20)
  })
})
import api from "../framework/services/index"

/*
*  API tests Airport Gap - https://airportgap.dev-tester.com/
*/

let token = ''


describe('/tokens', () => {
  test('Получение токена при правильном логине и пароле', async () => {
    const body = {email: 'test@airportgap.com', password: 'airportgappassword'}
    const res = await api().token().post(body)
    token = res.body.token
    expect(res.status).toBe(200)
    expect(token).not.toBeFalsy()
  })
  test('Получение ошибки авторизации при правильном логине и НЕ правильном пароле', async () => {
    const body = {email: 'test@airportgap.com', password: 'false'}
    const res = await api().token().post(body)
    expect(res.status).toBe(401)
    expect(res.body.errors[0].title).toBe('Unauthorized')
  })
})

describe('/favorites', () => {
  let id = 0
  test('Возвращает все избранные аэропорты', async () => {
    const res = await api().favorites().get(token)
    expect(res.status).toBe(200)
    expect(Array.isArray(res.body.data)).toBe(true)
    expect(res.body.data[0].type).toBe('favorite')
  })
  test('Позволяет сохранить любимый аэропорт', async () => {
    const res = await api().favorites().post(token, {'airport_id': 'JFK'})
    id = res.body.data.id
    expect(res.status).toBe(201)
  })
  test('Возвращает избранный аэропорт с указанным идентификатором', async () => {
    const res = await api().favorites().get(token, id)
    expect(res.status).toBe(200)
    expect(res.body.data instanceof Object).toBe(true)
    expect(res.body.data.id).toBe(id)
  })
  test('Позволяет обновлять заметку об одном из ваших любимых аэропортов', async () => {
    const res = await api().favorites().patch(token, id, {'note': 'Тестовая заметка'})
    expect(res.body.data.attributes.note).toBe('Тестовая заметка')
    expect(res.status).toBe(200)
  })
  test('Удаляет один из ваших любимых аэропортов', async () => {
    const res = await api().favorites().delete(token, id)
    expect(res.status).toBe(204)
  })
})
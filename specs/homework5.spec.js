import supertest from "supertest";

const URI = 'https://airportgap.dev-tester.com'

describe('GET. /api/airports', () => {
  const id = 'GKA'
  const PATH = `/api/airports/`
  let res = ''

  test('Получил cтатус 200', async () => {
    res = await supertest(URI)
      .get(PATH)
      .set('Accept', 'application/json')
    expect(res.status).toEqual(200)
  })
  test('Получил 30 аэропортов', async () => {
    expect(res.body.data.length).toEqual(30)
  })
  test('Получил аэропорт из списка с id = "GKA"', async () => {
    const airoport = res.body.data.find(item => item.id === `${id}`)
    expect(airoport.id).toEqual(`${id}`)
  })
})

describe('GET. /api/airports:id', () => {
  let id = 'GKA'
  const PATH = `/api/airports/`
  let res = ''
  
  test('Получил cтатус 200', async () => {
    res = await supertest(URI)
      .get(PATH+`${id}`)
      .set('Accept', 'application/json')
    expect(res.status).toEqual(200)
  })
  test('Получил аэропорт c id = GKA', async () => {
    expect(res.body.data.id).toEqual(`${id}`)
  })
  test('Получил cтатус 404, запрос c несушествущим id', async () => {
    id = 'bzzz'
    const res = await supertest(URI)
      .get(PATH+`${id}`)
      .set('Accept', 'application/json')
    expect(res.status).toEqual(404)
  })
})
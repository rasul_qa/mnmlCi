exports.runningKolobok = (person) => {
  switch (person) {
    case 'дедушка':
      return 'Я от дедушки ушёл';
    case 'бабушка':
      return 'Я от бабушки ушёл';
    case 'заяц':
      return 'От тебя, зайца, подавно уйду!';
    default:
      return 'Я не знаю такого персонажа :(';
  }
};

exports.newYear = (person) => {
  return `${person}! ${person}! ${person}!`;
};